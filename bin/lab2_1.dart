import 'package:lab2_1/lab2_1.dart' as lab2_1;
import 'dart:io';

bool isPrime(n) {
  for (int i=2; i<=n/2; i++) {
  if (n%i == 0) {
    return false;
    } 
  }
    return true;
}
void main() {
  print('Enter number');
  int n = int.parse(stdin.readLineSync()!);
  if (isPrime(n)) {
    print('$n is a prime number');
  } else {
    print('$n is not a prime number');
  }
}
